## 1.0.8
* Uploading label changed from "**Uploading, please do not move the camera**" > "**Keep tire in focus while scanning left to right.**".

## 1.0.7
* Plugin now can fetch the PDF result from a scan using the new getTreadDepthPdfResult(uuid) function.
* UI improvements.

## 0.0.1

* TODO: Describe initial release.
