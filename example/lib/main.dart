import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_anyline_tire_tread_scanner/flutter_anyline_tire_tread_scanner.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Material App',
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({
    super.key,
  });

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String _uuid = '';
  bool _loading = false;

  static const _kAnimationDuration = Duration(
    milliseconds: 200,
  );

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight, DeviceOrientation.landscapeLeft]);
  }

  @override
  Widget build(BuildContext context) {
    AnylineTireTreadScanner.onScanningEvent.listen((event) {
      if (event is ScanningAborted) {
        debugPrint(event.uuid);
      } else if (event is UploadAbortedEvent) {
        debugPrint(event.uuid);
      } else if (event is UploadCompletedEvent) {
        debugPrint(event.uuid);
        setState(() => _uuid = event.uuid ?? '');
      } else if (event is UploadFailedEvent) {
        debugPrint(event.error);
      }
    });

    return Scaffold(
      appBar: AppBar(
        title: const Text('Material App Bar'),
      ),
      body: Center(
        child: Column(
          children: [
            MaterialButton(
              onPressed: () async {
                try {
                  setState(() => _loading = true);
                  await AnylineTireTreadScanner.setup(
                    licenseKey: "{YOUR_LICENSE_KEY}",
                  );
                  setState(() => _loading = false);
                } catch (e) {
                  debugPrint(e.toString());
                  setState(() => _loading = false);
                }
              },
              child: const Text('Setup'),
            ),
            const SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: () async {
                try {
                  setState(() => _loading = true);
                  await AnylineTireTreadScanner.open();
                  setState(() => _loading = false);
                } catch (e) {
                  debugPrint(e.toString());
                  setState(() => _loading = false);
                }
              },
              child: const Text('Open'),
            ),
            const SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: (_loading)
                  ? null
                  : () async {
                      try {
                        setState(() => _loading = true);
                        final result = await AnylineTireTreadScanner.getTreadDepthResult(
                          uuid: _uuid,
                        );
                        debugPrint(result?.toString());
                        setState(() => _loading = false);
                      } catch (e) {
                        debugPrint(e.toString());
                        setState(() => _loading = false);
                      }
                    },
              child: const Text('Get scanner result'),
            ),
            const SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: (_loading)
                  ? null
                  : () async {
                      try {
                        setState(() => _loading = true);
                        final bytes = await AnylineTireTreadScanner.getTreadDepthPdfResult(
                          uuid: _uuid,
                        );
                        debugPrint(bytes?.toString());
                        setState(() => _loading = false);
                      } catch (e) {
                        debugPrint(e.toString());
                        setState(() => _loading = false);
                      }
                    },
              child: const Text('Get scanner pdf result'),
            ),
            const SizedBox(
              height: 20,
            ),
            AnimatedSwitcher(
              duration: _kAnimationDuration,
              child: (_loading)
                  ? const SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator.adaptive(),
                    )
                  : const SizedBox.shrink(),
            ),
          ],
        ),
      ),
    );
  }
}
